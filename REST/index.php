<?php
/**
*	ICED REST API
*	Andrew Sowers - Ithaca College
*	NOVEMBER 2014
*
*	RETURN SCHEMA=>
*	return types are JSON objects containing arrays or arrays of dictionaries
*
**/

    
include_once "rest.php";


/*
*	RestAPI
*
*	the RESTful API for interfacing with the front end app and backend db
*
*/
class RestAPI {
    
    // Main database object
    private $db;
    
    private $term = 200720;
    
   
    // Constructor - open DB connection
    function __construct() {
        $this->db = new mysqli('localhost', 'asowers1', 'ICED', 'ICED');
        $this->db->autocommit(TRUE);
    }
 
    // Destructor - close DB connection
    function __destruct() {
        $this->db->close();
    }
 
    
    private function schoolQueryTool(&$nodes,&$links,&$nodeIndex,&$linkIndex,&$nodeLookup,$school,$group,$parent,$term_id,$value){
	    $stmt = $this->db->prepare('SELECT DISTINCT crsdept, school, term_term_id FROM course WHERE school = "'.$school.'" AND term_term_id="'.$term_id.'" GROUP BY crsnum');
	   	$stmt->execute();
	   	$stmt->bind_result($crsdept,$school,$term_term_id);
		
		while ($stmt->fetch()) {
			$nodeLookup[$crsdept]=$nodeIndex;
			$nodes[$nodeIndex] = array("node"=>$nodeIndex,"name"=>$crsdept,"school"=>$school,"term_id"=>$term_id,"group"=>$group,"show"=>false,"misc"=>$school);
			$links[$linkIndex] = array("source"=>$linkIndex+1,"target"=>$parent,"value"=>$value,"show"=>false);
			$nodeIndex++;
			$linkIndex++;
			$value+=4;
		}
	    $stmt->close();
    }
    
    
    private function courseQueryTool(&$nodes,&$links,&$nodeIndex,&$linkIndex,&$nodeLookup,$school,$group,$term_id,$value){
	    $stmt = $this->db->prepare('SELECT DISTINCT crnx, crsid, crsdept, crsnum, secnum,term_term_id, school FROM course WHERE school = "'.$school.'" AND term_term_id="'.$term_id.'"');
		$stmt->execute();
		$stmt->bind_result($crnx,$crsid,$crsdept,$crsnum,$secnum,$term_term_id,$school);
		/* fetch values */
		
		while($stmt->fetch()) {
			$nodes[$nodeIndex] = array("misc"=>$school,"department"=>$crsdept,"crnx"=>$crnx,"node"=>$nodeIndex,"name"=>$crsid,"crsdept"=>$crsdept,"crsnum"=>$crsnum,"secnum"=>$secnum,"term_id"=>$term_term_id,"school"=>$school,"group"=>$group,"show"=>false);
			$links[$linkIndex] = array("source"=>$linkIndex+1,"target"=>$nodeLookup[$crsdept],"value"=>$value,"show"=>false);
			$nodeIndex++;
			$linkIndex++;
			$value+=4;
		}
	    $stmt->close();
		return $i;
    }
    
    /*
	*	analyze
	*
	*	analyze source/dest node sets
	*
	*	@return analyzation data
	*/
    function analyze(){
		
		// original source/destination context
		$data = $_POST["data"];
		
		// databucket holds filtered context
		$dataBucket = array("source"=>array(),"destination"=>array());
		$c = 0;
		
		// sift through context, update into dataBucket
		foreach($data as &$set){
			foreach($set as &$node){
				$group = $node["group"];
				if($group == 1){
					$id = $node["misc"];
				}else{
					$id = $node["id"];
				}
				$term = $node["term_id"];
				$index = $node["node"];
				
				// if source or destination node, filter for each, push into dataBucket
				if($c == 0){
					$type = "source";
					$info = array($group, $id, $term, $index, $type);
					array_push($dataBucket["source"],$info);
				}else{
					$type = "destination";
					$info = array($group, $id, $term, $index, $type);
					array_push($dataBucket["destination"],$info);
				}
			}
			$c++;
		}
		
		// out destination
		$contextToBeReturned = array();
		$finalResultDest = array();
		$finalResultSource = array();
		$finalResult = array();
		/*
			KEY -> d[0]:node type (parent/child hierarchy: 1 == school, 2 == crsdept, 3 == crsid),  	
		*/
		
		// for each destination node
		
		foreach($dataBucket["destination"] as &$d){
			if($d[0]==1){
				array_push($finalResultDest, $this->getStudentsFromSelection(&$d,&$contextToBeReturned, "school"));
			}else if($d[0]==2){
				array_push($finalResultDest, $this->getStudentsFromSelection(&$d,&$contextToBeReturned, "crsdept"));
			}else if($d[0]==3){
				array_push($finalResultDest, $this->getStudentsFromSelection(&$d,&$contextToBeReturned, "crsid"));
			}
		}
		
		// for each source node
		foreach($dataBucket["source"] as &$n){
			if($n[0]==1){
				array_push($finalResultSource, $this->getStudentsFromSelection(&$n, &$contextToBeReturned, "school"));
			}else if($n[0]==2){
				array_push($finalResultSource, $this->getStudentsFromSelection(&$n, &$contextToBeReturned, "crsdept"));
			}else if($n[0]==3){
				array_push($finalResultSource, $this->getStudentsFromSelection(&$n, &$contextToBeReturned, "crsid"));
			}
		}		
		
		
		foreach($finalResultSource as $sourceArray){
			foreach($sourceArray as $i){
				foreach($finalResultDest as $destArray){
					if(in_array($i, $destArray)){
							array_push($finalResult, $i);
					}
				}
			}
		}
		
		$finalResultMessy = array_unique($finalResult);
		$finalResultClean = array();
		foreach ($finalResultMessy as $index){
			array_push($finalResultClean, $index);
		}
		$finalResult = &$finalResultClean;

		
		//array_push($finalResult,$finalResultSource);
		//array_push($finalResult,$finalResultDest);

		
		
		// headers for not caching the results
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 2020 05:00:00 GMT');
		
		sendResponse(200, json_encode($finalResult),'application/json');

		return true;
	}
	
	
	/*
	function analyzeDestQuery(&$d,&$destArray, $type){
		$q = $this->db->prepare("SELECT crnx FROM course WHERE ".$type."='".$d[1]."' AND term_term_id='".$d[2]."'");
		$q->execute();
		$q->bind_result($crnx);
		while($q->fetch()){
			array_push($destArray,array($crnx,$d["index"]));
		}
		$q->close();
	}
	*/
	function getStudentsFromSelection(&$d, &$destArray, $type){
		$crnxArray = array();
		$q = $this->db->prepare("SELECT crnx FROM course WHERE ".$type."='".$d[1]."' AND term_term_id='".$d[2]."'");
		$q->execute();
		$q->bind_result($crnx);
		while($q->fetch()){
			array_push($crnxArray,array($crnx,$n["index"]));
		}
		
		$q->close();
		
		$studentArray = array();
		$stmt = "";
		$stmt = "SELECT student_fakeid FROM student_has_course WHERE course_crnx=";
		foreach ($crnxArray as $index) {
			$stmt = $stmt . "'" . $index[0] . "' OR course_crnx=";
		}
		$stmt = $stmt . '-1';
		$q = $this->db->prepare($stmt);
		$q->execute();
		$q->bind_result($student_fakeid);
		while($q->fetch()){
			array_push($studentArray,$student_fakeid);
		}
		$q->close();
		$studentArrayMessy = array_unique($studentArray);
		$studentArrayClean = array();
		foreach ($studentArrayMessy as $index){
			array_push($studentArrayClean, $index);
		}
		$studentArray = &$studentArrayClean;
		
		/*
		for($z = 0; $z < count($destArray); $z++){
			$stmt = "SELECT count(*) FROM student_has_course WHERE (student_fakeid=";
			for ($y = 0; $y < count($studentArray); $y++) {
				$stmt = $stmt . "'" . $studentArray[$y] . "'";
				if($studentArray[$y + 1] != NULL){
					$stmt = $stmt . " OR student_fakeid=";
				}
			}
			$stmt = $stmt . ") AND course_crnx='".$destArray[$z][0]."'";
			
			$q = $this->db->prepare($stmt);
			$q->execute();
			$q->bind_result($counts);
			while($q->fetch()){
				array_push($studentArray,$counts);
			}
			$q->close();
			
		}
		*/
		return $studentArray;
	}
 
 
 
    /*
    *	getAllTerms
    *
    *	we probably don't need this(maybe), it was just for testing purposes
    *
    *	@return array[array[term_id,terma],...]
    */
    function getAllTerms() {
		$output = array();
	
		$stmt = $this->db->prepare('SELECT * FROM term');
		$stmt->execute();
		$stmt->bind_result($term_id,$terma);
		/* fetch values */
		
		for ($i=0;$stmt->fetch();$i++) {
			$output[$i] = array($term_id,$terma);
		}
	    $stmt->close();
		
		// headers for not caching the results
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 2020 05:00:00 GMT');
		
		sendResponse(200, json_encode($output),'application/json');

		return true;

    }
    
    
    /*
	*	getInitialNodeSet
	*
	*	gets all nodes/edges and such for initial visualization data set
	*
	*	This code is static and pretty much terrible/ugly, but it's for prototyping purposes
	*
	*	@return jsonObject[nodes[node[...]],links[link[...]]];
	*/
	function initialListSet(){
		$nodes     = array(array("node"=> 0,"name"=>"Ithaca College",                       "misc"=>"HS","group"=>0,"show"=>true),
							array("node"=>1,"name"=>"Park School of Business",              "misc"=>"BU","group"=>1,"show"=>true,"term_id"=>$this->term),
							array("node"=>2,"name"=>"Park Communications",                  "misc"=>"CO","group"=>1,"show"=>true,"term_id"=>$this->term),
							array("node"=>3,"name"=>"Health Sciences and Human Performance","misc"=>"HH","group"=>1,"show"=>true,"term_id"=>$this->term),
							array("node"=>4,"name"=>"Humanities and Sciences",              "misc"=>"HS","group"=>1,"show"=>true,"term_id"=>$this->term),
							array("node"=>5,"name"=>"School of Music",                      "misc"=>"MU","group"=>1,"show"=>true,"term_id"=>$this->term));
		
		$links     = array(array("source"=>1,"target"=>0,"value"=>10,"show"=>true),
							array("source"=>2,"target"=>0,"value"=>10,"show"=>true),
							array("source"=>3,"target"=>0,"value"=>10,"show"=>true),
							array("source"=>4,"target"=>0,"value"=>10,"show"=>true),
							array("source"=>5,"target"=>0,"value"=>10,"show"=>true));
							
		$nodeIndex = 6; // beyond 6 is dynamically called via the database
		$linkIndex = 5;
		
		$nodeLookup = array();
		
		
		$schoolValues = array(
			1 => "BU",
			2 => "CO",
			3 => "HH",
			4 => "HS",
			5 => "MU",	
		);
		
		for($i=0;$i<=5;$i++){
			$this->schoolQueryTool(&$nodes,&$links,&$nodeIndex,&$linkIndex,&$nodeLookup,$schoolValues[$i],2,$i,$this->term,2);	//
		}
		
		for($i=0;$i<=5;$i++){
			$this->courseQueryTool(&$nodes,&$links,&$nodeIndex,&$linkIndex,&$nodeLookup,$schoolValues[$i],3,$this->term,2);	
		}
		
		// headers for not caching the results
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 2010 05:00:00 GMT');

		$payload = array("nodes"=>$nodes,"links"=>$links,"nodeLookup"=>$nodeLookup);				// build the data payload
		sendResponse(200, json_encode($payload),'application/json'); 	// send it to the body and REST response is OK

		return true;	//the true true
	}
	
	function testGetCourses(){
		// courseQueryTool(&$nodes,&$links,$nodeIndex,$school,$group,$term_id)
		$nodes = array();
		$links = array();
		
		$nodeIndex = 0;
		$linkIndex = 1;
		
		$this->courseQueryTool(&$nodes,&$links,&$nodeIndex,&$linkIndex,"HS", 3,$this->term,2);
		// headers for not caching the results
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 2010 05:00:00 GMT');

		$payload = array("nodes"=>$nodes,"links"=>$links);				// build the data payload
		sendResponse(200, json_encode($payload),'application/json'); 	// send it to the body and REST response is OK

		return true;	//the true true		
		
	}
	

    
       // end of RestAPI class
}



 
// This is the first thing that gets called when this page is loaded
// Creates a new instance of the RestAPI class and executes the rest method in the $_REQUEST super global array
$api = new RestAPI;
if($_REQUEST["call"]!=null){
	$api->$_REQUEST["call"]();
}else{
	sendResponse(400, json_encode(array("status"=>"failed")),'application/json');
	return true;
}

?>
